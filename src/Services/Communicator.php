<?php

namespace Drupal\neo4j\Services;

use Drupal\Core\Config\ConfigFactoryInterface;
use GraphAware\Neo4j\Client\ClientBuilder;
use PTS\Bolt\Configuration;

/**
 * Uses for providing connection to database.
 */
class Communicator {

  /**
   * Host.
   *
   * @var string
   */
  private $host;

  /**
   * Port.
   *
   * @var int
   */
  private $port;

  /**
   * Username.
   *
   * @var string
   */
  private $username;

  /**
   * Password.
   *
   * @var string
   */
  private $password;

  /**
   * TLS Encryption.
   *
   * @var bool
   */
  private $tls;

  /**
   * Provides credentials for database connection.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Configuration manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $configuration = $config_factory->get('neo4j.connection');
    $this->host = $configuration->get('host');
    $this->port = $configuration->get('port');
    $this->username = $configuration->get('user');
    $this->password = $configuration->get('pass');
    $this->tls = $configuration->get('tls');
  }

  /**
   * Provides connection to database.
   */
  public function connect() {
    $connection = "bolt://{$this->username}:{$this->password}@{$this->host}:{$this->port}";
    $config = NULL;
    if ($this->tls) {
      $config = Configuration::create()
        ->withTLSMode(Configuration::TLSMODE_REQUIRED);
    }
    return ClientBuilder::create()
      ->addConnection('bolt', $connection, $config)
      ->build();
  }

}
