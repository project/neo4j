<?php

namespace Drupal\neo4j\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use GraphAware\Neo4j\Client\ClientBuilder;
use PTS\Bolt\Configuration;

/**
 * Form for neo4j credentials.
 */
class AdminConfig extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['neo4j.connection'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'neo4j_admin_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('neo4j.connection');
    $host = $config->get('host');
    $user = $config->get('user');
    $port = $config->get('port');
    $tls = $config->get('tls');

    $form['user'] = [
      '#title' => $this->t('Username'),
      '#type' => 'textfield',
      '#default_value' => $user ?? '',
      '#required' => TRUE,
    ];

    $form['pass'] = [
      '#title' => $this->t('Password'),
      '#type' => 'password',
      '#required' => TRUE,
    ];

    $form['host'] = [
      '#title' => $this->t('Host'),
      '#type' => 'textfield',
      '#default_value' => $host ?? '',
      '#required' => TRUE,
    ];

    $form['port'] = [
      '#title' => $this->t('Port'),
      '#type' => 'textfield',
      '#default_value' => $port ?? '',
      '#required' => TRUE,
    ];

    $form['tls'] = [
      '#title' => $this->t('TLS'),
      '#type' => 'checkbox',
      '#default_value' => $tls,
    ];


    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $user = $form_state->getValue('user');
    $pass = $form_state->getValue('pass');
    $host = $form_state->getValue('host');
    $port = $form_state->getValue('port');
    $tls = $form_state->getValue('tls');
    $url = "bolt://{$user}:{$pass}@{$host}:{$port}";

    $result = NULL;
    $errorMessage = NULL;
    $config = NULL;

    try {
      if ($tls) {
        $config = Configuration::create()
          ->withTLSMode(Configuration::TLSMODE_REQUIRED);
      }
      $client = ClientBuilder::create()
        ->addConnection('bolt', $url, $config)
        ->build();
      $result = $client->run('MATCH (n) RETURN n LIMIT 0');
    }
    catch (\Exception $e) {
      $errorMessage = $e->getMessage();
    }


    if (!$result) {
      $form_state->setErrorByName('', $errorMessage);
    }
    $form_state->set('host', $host)
      ->set('user', $user)
      ->set('port', $port)
      ->set('pass', $pass)
      ->set('tls', $tls);

    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('neo4j.connection')
      ->set('host', $form_state->get('host'))
      ->set('port', $form_state->get('port'))
      ->set('user', $form_state->get('user'))
      ->set('pass', $form_state->get('pass'))
      ->set('tls', $form_state->get('tls'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
